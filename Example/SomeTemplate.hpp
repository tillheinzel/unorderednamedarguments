#pragma once

#include <type_traits>

#include <named_type.hpp>

#include "AnyOrder/template/pickID.hpp"
#include "AnyOrder/template/AnyOrderFunctionTemplate.hpp"

/// Named Types
template<class T>
using LHS = fluent::NamedType<T, struct LHSID>;
constexpr LHS<int>::argumentTemplate lhs;

template<class T>
using RHS = fluent::NamedType<T, struct RHSID>;
constexpr RHS<int>::argumentTemplate rhs;

/// approximate FluentCpp implementation
template<class T1, class T2>
bool equals_impl(LHS<T1> const& a, RHS<T2> const& b)
{
	return a.get() == b.get();
}

template<class... Ts>
decltype(auto) equals(Ts&&... ts)
{
	auto args = std::tuple<Ts...>(ts...);
	return equals_impl(tillh::pickID<LHSID>(args), tillh::pickID<RHSID>(args));
}

/// AnyOrderFunction implementation
using Equals2 = tillh::AnyOrderFunctionTemplate<struct Equals2ID, bool(LHSID, RHSID)>;

template<>
struct Equals2::Impl
{
	template<class T1, class T2>
	bool operator() (LHS<T1> const& a, RHS<T2> const& b) const
	{
		return (a.get() - b.get())<1e-16;
	}
};

constexpr Equals2 equals2{};
