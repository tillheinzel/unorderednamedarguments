#pragma once

#include <type_traits>
#include <tuple>
#include <optional>

#include "AnyOrder/template/pickID.hpp"

namespace tillh
{
    template<class, class>
    class AnyOrderFunctionTemplate;

    template<class ID, class Ret, class... IDs>
    class AnyOrderFunctionTemplate<ID, Ret(IDs...)>
    {
    public:
        constexpr AnyOrderFunctionTemplate() = default;

        struct Impl;

        template<class... Args2>
        Ret operator()(Args2&&... args2) const
        {
            auto argTuple = std::tuple<Args2...>(args2...);
            return Impl()(pickID<std::decay_t<IDs>>(argTuple)...);
        }
    };
}
