#pragma once

namespace tillh::internal
{
    template<class T>
    struct Type
    {
        using type = T;
    };

    template<class... Ts>
    struct TypeList {};

    template<class T, class... Ts>
    using Front = T;
}
