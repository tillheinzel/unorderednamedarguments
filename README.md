# UnorderedNamedArguments

Small project with example (no tests) for creating global functions with 
strong arguments, that can be called in any order, based on the blogpost
https://www.fluentcpp.com/2018/12/14/named-arguments-cpp/

This thing can be used to create free functions where the arguments can be called in any order,
by following the examples in CoolName (which also has reference implementation 
from the blog post) and gauss. This means creating a global constant instead of 
the actual function, using an IDStruct as in https://www.fluentcpp.com/2016/12/08/strong-types-for-strong-interfaces/
to distinguish individual functions, and explicitly instantiate the impl-function of the 
corresponding type. 

The global variable can be constexpr if all the parameters values (which can be passed to the ctor) are literal types (so, sadly, no strings). 
using defaults with non-literal parameters will probably lead to some runtime overhead. Calling the function without defaults should not, due to constexpr branching.
