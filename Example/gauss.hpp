#pragma once
#include <utility>
#include <named_type.hpp>

#include "AnyOrder/AnyOrderFunction.hpp"

namespace Gauss
{
    using Mean = fluent::NamedType<double, struct MeanID>;
    using StdDev = fluent::NamedType<double, struct StdDevID>;
    using XVal = fluent::NamedType<double, struct XValID>;
}

constexpr Gauss::Mean::argument mean;
constexpr Gauss::StdDev::argument stdDev;
constexpr Gauss::XVal::argument xVal;

using GaussF = tillh::AnyOrderFunction<struct GaussID, double(Gauss::Mean, Gauss::StdDev, Gauss::XVal)>;
constexpr GaussF gauss{0.0, 1.0, tillh::noDefault};
