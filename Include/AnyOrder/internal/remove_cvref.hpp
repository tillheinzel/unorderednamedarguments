#pragma once
#include <type_traits>

namespace tillh::internal
{
    template<class T>
    struct remove_cvref
    {
        using type = std::remove_const_t<std::remove_reference_t<T>>;
    };

    template<class T>
    using remove_cvref_t = typename remove_cvref<T>::type;
}
