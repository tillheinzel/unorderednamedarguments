#pragma once

#include "AnyOrder/internal/NeverDefault.hpp"
#include "AnyOrder/internal/remove_cvref.hpp"

namespace tillh::internal
{
    struct NoDefault {};

    template<class NamedArg>
    struct DefaultParameter
    {
        static const bool isNeverDefault = false;

        using BaseParam = remove_cvref_t<NamedType_t<NamedArg>>;

        template<class... CtorParams, typename = std::enable_if_t<std::is_constructible_v<BaseParam, CtorParams...>>>
        constexpr DefaultParameter(CtorParams&&... ctorParams)
            :
            DefaultParameter(BaseParam(std::forward<CtorParams>(ctorParams)...))
        {}

        constexpr DefaultParameter(BaseParam parameter) : val(parameter)
        {}
        constexpr DefaultParameter(NoDefault) : val(std::nullopt)
        {}

        std::optional<decayNamedType_t<NamedArg>> val;
    };
    
    template<class NamedArg>
    struct DefaultParameter<NeverDefault<NamedArg>>
    {
        constexpr DefaultParameter(NoDefault)
        {}

        static const bool isNeverDefault = true;
    };
}
