#pragma once

#include <type_traits>

#include "AnyOrder/internal/TypeList.hpp"

namespace tillh::internal
{
    template<typename T, typename... Ts>
    constexpr bool contains(TypeList<Ts...>)
    {
        return std::disjunction_v<std::is_same<T, Ts>...>;
    }

    /// test
    static_assert(contains<int>(TypeList<int, bool, double>()));
    static_assert(!contains<int>(TypeList<bool, double>()));
    static_assert(!contains<int>(TypeList<>()));
}
