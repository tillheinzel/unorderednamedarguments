#include <iostream>

#include "CoolName.hpp"
#include "gauss.hpp"
#include "SomeTemplate.hpp"

int main()
{
	//std::cout << "BARMAN: What will it be, Mr. ... \n";

    using namespace coolName;
	//coolName1(firstName = "James", lastName = "Bond", drink = "Vodka Martini", preparation = PREPARATION::SHAKEN);
	coolName2(firstName = "James", drink = "Vodka Martini", preparation = PREPARATION::STIRRED, lastName = "Bond");
    //coolNameSimple.impl("James", "Bond");
	//coolName2(firstName = "James", lastName = "Bond", drink = "Vodka Martini", preparation = PREPARATION::SHAKEN);
	//coolName2(firstName = "James", lastName = "Bond");

 //   coolName2(3);

	///// example where parameters are literals, so can create constexpr object
	//const auto gaussVal = gauss(xVal = 0.0, stdDev = 2.0, mean = 1.0);
	//std::cout << "\nBOND: In case you where wondering, the value of the gaussian with mean "
	//	<< 1.0 << ", standard deviation " << 2.0 << ", at x = " << 0.0 << " is " << gaussVal << "\n";

	//const auto gaussVal2 = gauss(xVal = 0.0);
	//std::cout << "BOND: And with defaults (mean = 0, standard deviation = 1), it is " << gaussVal2 << "\n";

	//const auto doesEqual = equals2(rhs = 1, lhs = 1.0);

	//std::cout << "\nBOND: Also, 1 " << (doesEqual? "equals":"does not equal") << " 1.0" << "\n";

#ifdef WIN32
	system("pause");
#endif
}
