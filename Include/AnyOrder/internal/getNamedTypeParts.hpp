#pragma once

#include <named_type.hpp>

namespace tillh::internal
{
    template<template<class> class... Ts>
    struct TemplateList {};

    template<class T>
    struct NamedTypeParts;

    template<class T, class ID, template<class> class... modifiers>
    struct NamedTypeParts<fluent::NamedType<T, ID, modifiers...>>
    {
        using type = T;
        using id = ID;
        using mods = TemplateList<modifiers...>;
    };

    template<class T>
    using NamedType_t = typename NamedTypeParts<T>::type;

    template<class T>
    using NamedType_id = typename NamedTypeParts<T>::id;


    template<class T>
    struct isNamedType : std::false_type {};

    template<class T, class ID, template<class> class... modifiers>
    struct isNamedType<fluent::NamedType<T, ID, modifiers...>> : std::true_type {};

    template<class T>
    constexpr bool isNamedType_v = isNamedType<T>::value;

    template<class T>
    struct decayNamedType;

    template<class T, class ID, template<class> class... modifiers>
    struct decayNamedType<fluent::NamedType<T, ID, modifiers...>>
    {
        using type = fluent::NamedType<std::decay_t<T>, ID, modifiers...>;
    };

    template<class T>
    using decayNamedType_t = typename decayNamedType<T>::type;
}
