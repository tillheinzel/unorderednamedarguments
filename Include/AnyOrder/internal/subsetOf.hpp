#pragma once

#include <type_traits>

#include "AnyOrder/internal/contains.hpp"
#include "AnyOrder/internal/hasDuplicates.hpp"

namespace tillh::internal
{


    template<class... SubElement, class... Super>
    constexpr bool subsetOf(TypeList<SubElement...> subSet, TypeList<Super...> superSet)
    {
        static_assert(!internal::hasDuplicates(subSet));
        static_assert(!internal::hasDuplicates(superSet));

        if constexpr(sizeof...(SubElement) == 0) return true;
        if constexpr(sizeof...(SubElement) > sizeof...(Super)) return false;

        return (internal::contains<SubElement>(superSet) && ...);
    }

    // empty set should be subset of everything
    static_assert(subsetOf(TypeList<>(), TypeList<>())); 
    static_assert(subsetOf(TypeList<>(), TypeList<int>()));

    // improper subset
    static_assert(subsetOf(TypeList<int>(), TypeList<int>()));

    //proper subset
    static_assert(subsetOf(TypeList<int, double>(), TypeList<int, double, bool>()));

    // proper subset out of order
    static_assert(subsetOf(TypeList<double, int>(), TypeList<int, double, bool>()));

    // 
    static_assert(!subsetOf(TypeList<double, float>(), TypeList<int, double, bool>()));
}
