#include "gauss.hpp"

#include <cmath>

const double PI = 3.14159;

template<>
double GaussF::impl(double theMean, double theStdDev, double x) const
{
	const auto frontFactor = 1.0 / std::sqrt(2 * PI*theStdDev);

	const auto expFactor = std::exp(-std::pow(x - theMean, 2) / (2 * theStdDev));

	return frontFactor * expFactor;
}
