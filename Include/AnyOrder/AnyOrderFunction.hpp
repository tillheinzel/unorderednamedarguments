#pragma once

#include <type_traits>
#include <tuple>
#include <optional>

#include "AnyOrder/internal/getNamedTypeParts.hpp"
#include "AnyOrder/internal/contains.hpp"
#include "AnyOrder/internal/hasDuplicates.hpp"
#include "AnyOrder/internal/DefaultParam.hpp"
#include "AnyOrder/internal/ensureFail.hpp"
#include "AnyOrder/internal/subsetOf.hpp"

namespace tillh
{
    constexpr internal::NoDefault noDefault;

    template<class, class F>
    class AnyOrderFunction
    {
        static_assert(internal::ensureFail<F>(), "Second parameter must be a function-format");
    };

    template<class ID, class Ret, class... Args>
    class AnyOrderFunction <ID, Ret(Args...)> {
        static_assert(std::conjunction_v<internal::isNamedType<internal::removeNeverDefault_t<Args>>...>, "all parameters must be NamedTypes");
        static_assert(!hasDuplicates(internal::TypeList<internal::removeNeverDefault_t<Args>...>()), "parameters must be unique");
    public:
        constexpr AnyOrderFunction() = default;

        constexpr AnyOrderFunction(internal::DefaultParameter<Args>... defaults) : defaults_(std::move(defaults)...)
        {}

        Ret impl(internal::NamedType_t<internal::removeNeverDefault_t<Args>>... args) const;

        template<class... Args2>
        Ret operator() (Args2... args2) const
        {
            static_assert(internal::subsetOf(internal::TypeList<Args2...>{}, internal::TypeList<internal::removeNeverDefault_t<Args>...>{}), "Some of the passed-in parameters do not have the correct type");
            
            auto argTuple = std::tuple<Args2...>(args2...);
            return impl(pickOrDefault<Args>(argTuple)...);
        }

    private:

        const std::tuple<internal::DefaultParameter<Args>...> defaults_;

        template<typename TypeToPick, typename... CallArgs>
        auto const& pickOrDefault(const std::tuple<CallArgs...>& args) const
        {
            static_assert(internal::contains<TypeToPick>(internal::TypeList<Args...>()));

            if constexpr(internal::contains<TypeToPick>(internal::TypeList<CallArgs...>()))
            {
                return std::get<TypeToPick>(args).get();
            }
            else
            {
                static_assert(!internal::DefaultParameter<TypeToPick>::isNeverDefault, "There can be no default value for this parameter");

                auto pickedDefault = std::get<internal::DefaultParameter<TypeToPick>>(defaults_).val;
                if(pickedDefault)
                {
                    return (*pickedDefault).get();
                }
                throw std::runtime_error{"no default for this type"};
            }
        }
    };
}
