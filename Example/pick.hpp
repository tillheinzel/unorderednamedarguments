#pragma once
#include <tuple>


template<typename TypeToPick, typename... Types>
TypeToPick pick(std::tuple<Types...>& args)
{
	return std::get<TypeToPick>(args);
}
