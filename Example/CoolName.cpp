#include "CoolName.hpp"

#include <iostream>

using namespace coolName;

void coolName1_impl(FirstName theFirstName, LastName theLastName, Drink theDrink, Preparation thePrep)
{
	const auto prepMiddle = ", not ";
	const auto prepStart = std::string{thePrep.get() == PREPARATION::SHAKEN ? "shaken" : "stirred"};
	const auto prepEnd = thePrep.get() == PREPARATION::SHAKEN ? "stirred" : "shaken";

	const auto prep = prepStart + prepMiddle + prepEnd;

	std::cout << "BOND: ";
	std::cout << theLastName.get() << ", " << theFirstName.get() << ' ' << theLastName.get() << ". " << theDrink.get() << " - " << prep << ".\n";
}

template<>
void CoolName2::impl(std::string const& theFirstName, std::string const& theLastName, std::string const& theDrink, PREPARATION thePrep) const
{
	const auto prepMiddle = ", not ";
	const auto prepStart = std::string{thePrep == PREPARATION::SHAKEN ? "shaken" : "stirred"};
	const auto prepEnd = thePrep == PREPARATION::SHAKEN ? "stirred":"shaken";

	const auto prep = prepStart + prepMiddle + prepEnd;

	std::cout << "BOND: ";
	std::cout << theLastName << ", " << theFirstName << ' ' << theLastName << ". " << theDrink << " - " << prep << ".\n";
}

template<>
void CoolNameSimple::impl(std::string const& theFirstName, std::string const& theLastName) const
{
	std::cout << theLastName << ", " << theFirstName << ' ' << theLastName << ".\n";
}
