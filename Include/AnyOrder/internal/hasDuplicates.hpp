#pragma once

#include "AnyOrder/internal/contains.hpp"

namespace tillh::internal
{
    constexpr bool hasDuplicates(TypeList<>)
    {
        return false;
    }

    template<class Head, class... Tail>
    constexpr bool hasDuplicates(TypeList<Head, Tail...>)
    {
        if constexpr(contains<Head>(TypeList<Tail...>()))
        {
            return true;
        }
        else
        {
            return hasDuplicates(TypeList<Tail...>());
        }
    }


    static_assert(!hasDuplicates(TypeList()));
    static_assert(!hasDuplicates(TypeList<int, bool, double>()));
    static_assert(hasDuplicates(TypeList<int, bool, int, double>()));
}
