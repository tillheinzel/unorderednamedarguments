#pragma once

namespace tillh
{
    template<class T>
    struct NeverDefault
    {
        using type = T;
    };
}

namespace tillh::internal
{

    template<class T>
    struct removeNeverDefault
    {
        using type = T;
    };
    template<class T>
    struct removeNeverDefault<NeverDefault<T>>
    {
        using type = T;
    };

    template<class T>
    using removeNeverDefault_t = typename removeNeverDefault<T>::type;

}
