#ifndef named_type_impl_h
#define named_type_impl_h

#include <type_traits>

// Enable empty base class optimization with multiple inheritance on Visual Studio.
#if defined(_MSC_VER) && _MSC_VER >= 1910
#  define FLUENT_EBCO __declspec(empty_bases)
#else
#  define FLUENT_EBCO
#endif

namespace fluent
{
    
template<typename T>
using IsNotReference = typename std::enable_if<!std::is_reference<T>::value, void>::type;

template <typename T, typename Parameter, template<typename> class... Skills>
class FLUENT_EBCO NamedType : public Skills<NamedType<T, Parameter, Skills...>>...
{
public:
    using UnderlyingType = T;
    
    // constructor
    explicit constexpr NamedType(T value) : value_(value) {}
    //template<typename T_ = T, typename = IsNotReference<T_>>
    //explicit constexpr NamedType(T&& value) : value_(std::move(value)) {}
    
    // get
    constexpr T& get() { return value_; }
    constexpr T const& get() const {return value_; }

    // conversions
    using ref = NamedType<T&, Parameter, Skills...>;
    operator ref ()
    {
        return ref(value_);
    }
    
    struct argument
    {
		constexpr NamedType operator=(T value) const
        {
            return NamedType(value);
        }
        argument() = default;
        ~argument() = default;
        argument(argument const&) = delete;
        argument(argument &&) = delete;
        argument& operator=(argument const&) = delete;
        argument& operator=(argument &&) = delete;
    };

	struct argumentTemplate
	{
		template<typename U>
		NamedType<U, Parameter, Skills...> operator=(U&& value) const
		{
			return NamedType<U, Parameter, Skills...>{std::forward<U>(value)};
		}
		argumentTemplate() = default;
		~argumentTemplate() = default;
		argumentTemplate(argumentTemplate const&) = delete;
		argumentTemplate(argumentTemplate &&) = delete;
		argumentTemplate& operator=(argumentTemplate const&) = delete;
		argumentTemplate& operator=(argumentTemplate &&) = delete;
	};


private:
    T value_;
};

template<template<typename T> class StrongType, typename T>
constexpr StrongType<T> make_named(T const& value)
{
    return StrongType<T>(value);
}
    
} // namespace fluent

#endif /* named_type_impl_h */
