#pragma once

#include <string>
#include <utility>

#include <named_type.hpp>
#include <AnyOrder/AnyOrderFunction.hpp>

#include "pick.hpp"

enum class PREPARATION {
    SHAKEN, STIRRED
};

/// Named Types. Avoid polluting global namespace, especially with the IDs
namespace coolName
{
    using FirstName = fluent::NamedType<std::string const&, struct FirstNameID>;
    using LastName = fluent::NamedType<std::string const&, struct LastNameID>;
    using Drink = fluent::NamedType<std::string const&, struct DrinkID>;
    using Preparation = fluent::NamedType<PREPARATION, struct PreparationID>;

    /// parameters for assignment-syntax
    constexpr FirstName::argument firstName;
    constexpr LastName::argument lastName;
    constexpr Drink::argument drink;
    constexpr Preparation::argument preparation;
}

/// approximate FluentCpp implementation
void coolName1_impl(coolName::FirstName theFirstName, coolName::LastName theLastName, coolName::Drink theDrink, coolName::Preparation thePrep);

template<class... Ts>
void coolName1(Ts... ts)
{
    auto args = std::tuple<Ts...>(ts...);
    coolName1_impl(pick<coolName::FirstName>(args), pick<coolName::LastName>(args), pick<coolName::Drink>(args), pick<coolName::Preparation>(args));
}

/// AnyOrderFunction implementation
using CoolName2 = tillh::AnyOrderFunction<struct CoolName2ID, void(coolName::FirstName, coolName::LastName, coolName::Drink, coolName::Preparation)>;
const CoolName2 coolName2{tillh::noDefault, tillh::noDefault, "Vodka Martini", PREPARATION::SHAKEN};

/// AnyOrderFunction with deactivated defaults, enabling constexpr construction.
using CoolName3 = tillh::AnyOrderFunction<struct CoolName3ID, void(tillh::NeverDefault<coolName::FirstName>, tillh::NeverDefault<coolName::LastName>, tillh::NeverDefault<coolName::Drink>, coolName::Preparation)>;
constexpr CoolName3 coolName3{tillh::noDefault, tillh::noDefault, tillh::noDefault, PREPARATION::SHAKEN};

using CoolNameSimple = tillh::AnyOrderFunction<struct CoolNameSimpleID, void(tillh::NeverDefault<coolName::FirstName>, tillh::NeverDefault<coolName::LastName>)>;
constexpr CoolNameSimple coolNameSimple{tillh::noDefault, tillh::noDefault};
