#pragma once

#include <type_traits>
#include <tuple>

#include "AnyOrder/internal/TypeList.hpp"

namespace tillh::internal
{
    template<template<class> class Filter, class Check, class... Ts >
    constexpr unsigned getIndex(unsigned I, Type<Check>, Type<Ts>... remainder)
    {
        if constexpr(Filter<Check>::value)
        {
            return I;
        }
        else
        {
            static_assert(sizeof...(Ts) != 0);

            return getIndex<Filter>(I + 1, remainder...);
        }
    }

    template<template<class> class Filter, class... Ts>
    auto getQualified(std::tuple<Ts...>& tuple)
    {
        constexpr unsigned FirstFulfilledIndex = getIndex<Filter>(0u, Type<Ts>{}...);
        return std::get<FirstFulfilledIndex>(tuple);
    }
}
