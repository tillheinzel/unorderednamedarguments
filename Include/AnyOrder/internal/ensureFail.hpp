#pragma once

#include <type_traits>

namespace tillh::internal
{
    template<class T>
    bool ensureFail()
    {
        return std::integral_constant<T, false>::value;
    }
}
